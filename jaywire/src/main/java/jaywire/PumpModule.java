package jaywire;

import coffee.Pump;
import coffee.Thermosiphon;

public interface PumpModule extends HeaterModule {
  default Pump getPump() {
    return new Thermosiphon(getHeater());
  }
}
