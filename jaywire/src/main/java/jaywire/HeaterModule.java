package jaywire;

import coffee.ElectricHeater;
import coffee.Heater;
import com.vanillasource.jaywire.SingletonScopeSupport;

public interface HeaterModule extends SingletonScopeSupport {
  default Heater getHeater() {
    return singleton(ElectricHeater::new);
  }
}
