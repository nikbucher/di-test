package jaywire;

import coffee.CoffeeMaker;
import com.vanillasource.jaywire.standalone.StandaloneModule;

public class CoffeeShopConfig extends StandaloneModule implements HeaterModule, PumpModule {
  public CoffeeMaker getCoffeeMaker() {
    return new CoffeeMaker(getHeater(), getPump());
  }
}
