package spring;

import coffee.CoffeeMaker;
import coffee.Heater;
import coffee.Pump;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CoffeeShopConfig {

  private final AnnotationConfigApplicationContext applicationContext;

  public CoffeeShopConfig() {
    applicationContext = new AnnotationConfigApplicationContext(PumpModule.class, HeaterModule.class);
    applicationContext.register(CoffeeMaker.class);
  }

  Pump getPump() {
    return applicationContext.getBean(Pump.class);
  }

  Heater getHeater() {
    return applicationContext.getBean(Heater.class);
  }

  CoffeeMaker getCoffeeMaker() {
    return applicationContext.getBean(CoffeeMaker.class);
  }

}
