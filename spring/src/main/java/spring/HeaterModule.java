package spring;

import coffee.ElectricHeater;
import coffee.Heater;
import org.springframework.context.annotation.Bean;

public class HeaterModule {
  @Bean
  public Heater heater() {
    return new ElectricHeater();
  }
}
