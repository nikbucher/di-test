package spring;

import coffee.Heater;
import coffee.Pump;
import coffee.Thermosiphon;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class PumpModule {
  @Bean
  @Scope("prototype")
  public Pump pump(Heater heater) {
    return new Thermosiphon(heater);
  }
}
