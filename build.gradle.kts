val javaVersion = libs.versions.java.get()

val kotlinBom = libs.kotlin.bom
val springBom = libs.spring.boot.bom
val junitJupiter = libs.junit.jupiter
val junitPlatformLauncher = libs.junit.platform.launcher
val assertj = libs.assertj

subprojects {

  apply<JavaPlugin>()

  dependencies {
    add("implementation", platform(kotlinBom))
    add("implementation", platform(springBom))
    add("testImplementation", junitJupiter)
    add("testRuntimeOnly", junitPlatformLauncher)
    add("testImplementation", assertj)
  }

  tasks {
    withType<JavaCompile> {
      sourceCompatibility = javaVersion
    }

    withType<Test> {
      useJUnitPlatform()
    }
  }

}
