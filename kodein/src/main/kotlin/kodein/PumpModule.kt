package kodein

import coffee.Pump
import coffee.Thermosiphon
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.provider

val pumpModule = DI.Module("Pump Module") {
  bind<Pump>() with provider { Thermosiphon(instance()) }
}
