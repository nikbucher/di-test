package kodein

import coffee.CoffeeMaker
import coffee.Heater
import coffee.Pump
import org.kodein.di.*

val coffeeShopConfig = DI {
  import(pumpModule)
  import(heaterModule)
  bind<CoffeeMaker>() with singleton { CoffeeMaker(instance(), instance()) }
}

class CoffeeShopConfig {
  fun getHeater(): Heater = coffeeShopConfig.direct.instance()
  fun getPump(): Pump = coffeeShopConfig.direct.instance()
  fun getCoffeeMaker(): CoffeeMaker = coffeeShopConfig.direct.instance()
}
