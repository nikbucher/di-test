package kodein

import coffee.ElectricHeater
import coffee.Heater
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.singleton

val heaterModule = DI.Module("Heater Module") {
  bind<Heater>() with singleton { ElectricHeater() }
}
