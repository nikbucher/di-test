package dagger;

import coffee.ElectricHeater;
import coffee.Heater;
import jakarta.inject.Singleton;

@Module
interface HeaterModule {
  @Singleton
  @Binds
  Heater to(ElectricHeater heater);
}
