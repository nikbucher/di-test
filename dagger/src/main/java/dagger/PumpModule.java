package dagger;

import coffee.Pump;
import coffee.Thermosiphon;

@Module
interface PumpModule {
  @Binds
  Pump to(Thermosiphon heater);
}
