package dagger;

import coffee.CoffeeMaker;

public class CoffeeApp {

  public static void main(String[] args) {
    CoffeeShopConfig coffeeShop = DaggerCoffeeShopConfig.builder().build();
    CoffeeMaker maker = coffeeShop.getCoffeeMaker();
    System.out.println("los gehts!");
    maker.brew();
  }
}
