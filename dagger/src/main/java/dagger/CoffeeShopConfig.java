package dagger;

import coffee.CoffeeMaker;
import coffee.Heater;
import coffee.Pump;
import jakarta.inject.Singleton;

@Singleton
@Component(modules = {HeaterModule.class, PumpModule.class})
public interface CoffeeShopConfig {

  Pump getPump();

  Heater getHeater();

  CoffeeMaker getCoffeeMaker();

}
