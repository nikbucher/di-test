dependencies {
  implementation(project(":core"))
  implementation(libs.jakarta.inject.api)
  implementation(libs.dagger)
  annotationProcessor(libs.dagger.compiler)
}
