package koin

import coffee.ElectricHeater
import coffee.Heater
import org.koin.dsl.module

val heaterModule = module {
  single<Heater> { ElectricHeater() }
}
