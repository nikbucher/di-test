package koin

import coffee.Pump
import coffee.Thermosiphon
import org.koin.dsl.module

val pumpModule = module {
  factory<Pump> { Thermosiphon(get()) }
}
