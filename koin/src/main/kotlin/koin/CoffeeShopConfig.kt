package koin

import coffee.CoffeeMaker
import coffee.Heater
import coffee.Pump
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val coffeeShopConfig = startKoin {
  modules(pumpModule)
  modules(heaterModule)
  modules(module {
    singleOf(::CoffeeMaker)
  })
}

class CoffeeShopConfig {
  fun getHeater(): Heater = coffeeShopConfig.koin.get()
  fun getPump(): Pump = coffeeShopConfig.koin.get()
  fun getCoffeeMaker(): CoffeeMaker = coffeeShopConfig.koin.get()
}
