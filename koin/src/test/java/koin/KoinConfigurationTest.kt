package koin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.PrintStream

internal class KoinConfigurationTest {
  private val sut = CoffeeShopConfig()
  private val outContent = ByteArrayOutputStream()
  private val errContent = ByteArrayOutputStream()

  @BeforeEach
  fun setUpStreams() {
    System.setOut(PrintStream(outContent))
    System.setErr(PrintStream(errContent))
  }

  @AfterEach
  fun cleanUpStreams() {
    System.setOut(null)
    System.setErr(null)
  }

  @Test
  fun should_always_return_the_same_heater() {
    // when
    val result1 = sut.getHeater()
    val result2 = sut.getHeater()
    // then
    assertThat(result1).isSameAs(result2)
  }

  @Test
  fun should_always_return_a_new_pump() {
    // when
    val result1 = sut.getPump()
    val result2 = sut.getPump()
    // then
    assertThat(result1).isNotSameAs(result2)
  }

  @Test
  fun should_return_coffeemaker_that_brews() {
    // when
    val result = sut.getCoffeeMaker()
    // then
    result.brew()
    assertThat(outContent.toString()).isEqualTo(
      """
      |~ ~ ~ heating ~ ~ ~
      |=> => pumping => =>
      | [_]P coffee! [_]P 
      |""".trimMargin()
    )
    assertThat(errContent.toString()).isEmpty()
  }

}
