rootProject.name = "di-test"

// https://docs.gradle.org/current/userguide/structuring_software_products.html
// https://docs.gradle.org/current/userguide/platforms.html
dependencyResolutionManagement {
  repositories {
    mavenCentral()
  }
}

include("core")
include("dagger")
include("guice")
include("jaywire")
include("kodein")
include("koin")
include("spring")
