package guice;

import static org.assertj.core.api.Assertions.assertThat;

import coffee.CoffeeMaker;
import coffee.Heater;
import coffee.Pump;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GuiceConfigurationTest {

  private final CoffeeShopConfig sut = new CoffeeShopConfig();
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

  @BeforeEach
  void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }

  @AfterEach
  void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }

  @Test
  void should_always_return_the_same_heater() {
    // when
    final Heater result1 = sut.getHeater();
    final Heater result2 = sut.getHeater();
    // then
    assertThat(result1).isEqualTo(result2);
  }

  @Test
  void should_always_return_a_new_pump() {
    // when
    final Pump result1 = sut.getPump();
    final Pump result2 = sut.getPump();
    // then
    assertThat(result1).isNotSameAs(result2);
  }

  @Test
  void should_return_coffeemaker_that_brews() {
    // when
    final CoffeeMaker result = sut.getCoffeeMaker();
    // then
    result.brew();
    assertThat(outContent.toString()).isEqualTo("""
        ~ ~ ~ heating ~ ~ ~
        => => pumping => =>
         [_]P coffee! [_]P\s
        """);
    assertThat(errContent.toString()).isEmpty();
  }
}
