package guice;

import coffee.CoffeeMaker;
import coffee.Heater;
import coffee.Pump;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class CoffeeShopConfig {

  private final Injector injector;

  public CoffeeShopConfig() {
    injector = Guice.createInjector(new HeaterModule(), new PumpModule());
  }

  Pump getPump() {
    return injector.getInstance(Pump.class);
  }

  Heater getHeater() {
    return injector.getInstance(Heater.class);
  }

  CoffeeMaker getCoffeeMaker() {
    return injector.getInstance(CoffeeMaker.class);
  }

}
