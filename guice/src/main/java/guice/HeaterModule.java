package guice;

import coffee.ElectricHeater;
import coffee.Heater;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class HeaterModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(Heater.class).to(ElectricHeater.class).in(Singleton.class);
  }
}
