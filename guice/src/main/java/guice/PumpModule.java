package guice;

import coffee.Pump;
import coffee.Thermosiphon;
import com.google.inject.AbstractModule;

public class PumpModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(Pump.class).to(Thermosiphon.class);
  }
}
