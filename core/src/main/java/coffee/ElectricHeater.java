package coffee;

import jakarta.inject.Inject;

public final class ElectricHeater implements Heater {
  private boolean heating;

  @Inject // because of Dagger 🙈
  public ElectricHeater() {
  }

  @Override
  public void on() {
    System.out.println("~ ~ ~ heating ~ ~ ~");
    this.heating = true;
  }

  @Override
  public void off() {
    this.heating = false;
  }

  @Override
  public boolean isHot() {
    return heating;
  }
}
