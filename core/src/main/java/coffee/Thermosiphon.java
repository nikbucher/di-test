package coffee;

import jakarta.inject.Inject;

public final class Thermosiphon implements Pump {
  private final Heater heater;

  @Inject // because of Dagger 🙈
  public Thermosiphon(Heater heater) {
    this.heater = heater;
  }

  @Override
  public void pump() {
    if (heater.isHot()) {
      System.out.println("=> => pumping => =>");
    }
  }
}
